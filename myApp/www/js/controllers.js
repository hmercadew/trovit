angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('CitiesCtrl', function($scope, $location, $state) {
  $scope.cities = [
    { title: 'Tarragona', id: 1 },
    { title: 'Barcelona', id: 2 },
    { title: 'Londres', id: 3 },
    { title: 'Paris', id: 4 },
    { title: 'Berlin', id: 5 },
    { title: 'Tokyo', id: 6 }
  ];

      $scope.search = function() {
      /* $location.path('/tab/newpost'); */   /* this variant doesnt work */
      //$scope.sitem = "HI";
      $state.go("app.single", {cityId: $scope.search.term}); // I hate you
    };

})

.controller('CityCtrl', function($scope, $http, $stateParams) {
  $http.get('http://api.openweathermap.org/data/2.5/weather?q=' + $stateParams.cityId + '&appid=155de3b370efd8e0f45c05494dfb08e9').then(function(resp) {
      $scope.city = resp.data;
    }, function(err) {
      // Will be undefined
    })
});
